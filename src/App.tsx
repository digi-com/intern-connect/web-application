import
  React
, { useEffect }from 'react';

import {
  Outlet,
  useNavigate
} from 'react-router-dom';

import {
  VContainer
} from './common';

export const App: React.FC = () => {

  const navigate = useNavigate();

  useEffect(() => {

    // navigate('/auth/login', { replace: true });    
  }, []);

  return (

    <VContainer
      $width='100vw'
      $height='100vh'
      $background='linear-gradient(#ffffff, #ffffff, #C2FF92)'>

      <Outlet />
    </VContainer>    
  )
}