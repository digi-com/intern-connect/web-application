import { NavLink } from "react-router-dom"
import { H1, VContainer } from "../../common"

export const Home: React.FC = () => {

  return (

    <VContainer>

      <H1
        text="Home" />

      <NavLink
        to={ '/auth/login' }>
      
        Login
      </NavLink>

      <NavLink
        to={ '/auth/register' }>
      
        Register
      </NavLink>
    </VContainer>
  )
}