import { useNavigate } from "react-router-dom"
import { H1, HContainer, PrimaryButton, VContainer, Text } from "../../common"
import { Input } from "../../common/components/Input"

export const ForgotPassword: React.FC = () => {

  const navigate = useNavigate();

  return (

    <VContainer
      $background="linear-gradient(#ffffff, #ffffff, #C2FF92)"
      $width="100vw"
      $height="100vh">

      <VContainer
        $margin="auto auto"
        $gap="1.25rem">

        <VContainer
          $gap="0.625rem">

          <H1
            $textAlign="center"
            text={ 'Forgot password' } />

          <Input
            placeholder="Intern Number/Email" />
        </VContainer>

        <PrimaryButton
          onClick={ () => navigate('/auth/reset-password') }
          text="Reset Password" />

        <VContainer
          $gap="0.625rem">

          <VContainer
            onClick={ () => navigate('/auth/login') }
            $hoverCursor="pointer">

            <Text
              $fontWeight="700"
              $textAlign="center">

              Login
            </Text>
          </VContainer>
        </VContainer>
      </VContainer>
    </VContainer>
  )
}