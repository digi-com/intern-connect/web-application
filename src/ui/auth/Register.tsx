import { useNavigate } from "react-router-dom"
import {
  H1,
  HContainer,
  PrimaryButton,
  Text,
  VContainer
} from "../../common"

import {
  Input
} from "../../common/components/Input"

export const Register: React.FC = () => {

  const navigate = useNavigate();

  return (

    <VContainer
      $background="linear-gradient(#ffffff, #ffffff, #C2FF92)"  
      $width="100vw"
      $height="100vh">

      <VContainer
        $margin="auto auto"
        $gap="1.25rem">

        <VContainer
          $gap="0.625rem">

          <H1
            $textAlign="center"
            text={ 'Register' } />

          <Input
            placeholder="First Name" />

          <Input
            placeholder="Last Name" />

          <Input
            placeholder="Email" />

          <Input
            placeholder="Contact Number" />

          <Input
            type="password"
            placeholder="Password" />

          <Input
            type="password"
            placeholder="Confirm Password" />
        </VContainer>

        <PrimaryButton
          onClick={ () => navigate('/onboarding') }
          text="Register" />

        <VContainer
          $gap="0.625rem">

          <VContainer
            $hoverCursor="pointer"
            onClick={ () => navigate('/auth/login') }>

            <Text
              $fontWeight="700"
              $textAlign="center">

              Login
            </Text>
          </VContainer>
        </VContainer>
      </VContainer>
    </VContainer>
  )
}
