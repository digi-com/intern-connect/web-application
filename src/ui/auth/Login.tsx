import {
  useNavigate
} from "react-router-dom"

import {
  H1,
  PrimaryButton,
  VContainer,
  Text,
  HContainer
} from "../../common"

import {
  Input
} from "../../common/components/Input"

export const Login: React.FC = () => {

  const navigate = useNavigate();

  return (

    <VContainer
      $background="linear-gradient(#ffffff, #ffffff, #C2FF92)"
      $width="100vw"
      $height="100vh">

      <VContainer
        $margin="auto auto"
        $gap="1.25rem">

        <VContainer
          $gap="0.625rem">

          <H1
            $textAlign="center"
            text={ 'Login' } />

          <Input
            placeholder="Intern Number/Email" />

          <Input
            type="password"
            placeholder="Password" />
        </VContainer>

        <PrimaryButton
          text="Connect" />

        <VContainer
          $gap="0.625rem">

          <VContainer
            onClick={ () => navigate('/auth/register') }
            $hoverCursor="pointer">

            <Text
              $fontWeight="700"
              $textAlign="center">

              Create an account
            </Text>            
          </VContainer>

          <VContainer
            onClick={ () => navigate('/auth/forgot-password') }
            $hoverCursor="pointer">

            <Text
              $fontWeight="700"
              $textAlign="center">

              Forgot password
            </Text>                
          </VContainer>
        </VContainer>
      </VContainer>
    </VContainer>
  )
}