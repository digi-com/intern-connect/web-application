import {
  H1,
  PrimaryButton,
  VContainer
} from "../../common"

import {
  Input
} from "../../common/components/Input"

export const ResetPassword: React.FC = () => {

  return (

    <VContainer
      $background="linear-gradient(#ffffff, #ffffff, #C2FF92)"
      $width="100vw"
      $height="100vh">

      <VContainer
        $margin="auto auto"
        $gap="1.25rem">

        <VContainer
          $gap="0.625rem">

          <H1
            $textAlign="center"
            text={ 'Reset Password' } />

          <Input
            type="password"
            placeholder="New Password" />

          <Input
            type="password"
            placeholder="Password" />
        </VContainer>

        <PrimaryButton
          text="Connect" />
      </VContainer>
    </VContainer>
  )
}