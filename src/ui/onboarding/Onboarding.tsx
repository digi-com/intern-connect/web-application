import {
  useNavigate
} from "react-router-dom"

import {
  H1,
  HContainer,
  PrimaryButton,
  Text,
  VContainer
} from "../../common"

export const Onboarding: React.FC = () => {

  const navigate = useNavigate();

  return (

    <VContainer
      $width="100vw"
      $height="100vh"
      $background="linear-gradient(#C8F4D3, #C8F4D3, #C2FF92)">

      <VContainer
        $gap="40px"
        $margin="auto auto">

        <H1
          $textAlign="center"
          $fontSize="48px"
          text="Welcome to Intern Connect" />

        <VContainer
          $margin="auto auto"
          $justifyContent="center">

          <HContainer
            $gap="0.625rem">

            <Text 
              $fontWeight="600"
              $fontStyle="italic"
              $fontSize="20px">

              "Share,
            </Text>

            <Text
              $fontWeight="600"
              $fontStyle="italic"
              $fontSize="20px">

              Connect,
            </Text>

            <Text
              $fontWeight="600"
              $fontStyle="italic"
              $fontSize="20px">

              Inspire"
            </Text>       
          </HContainer>          
        </VContainer>

        <VContainer
          $margin="auto auto">

          <PrimaryButton
            width="200px"
            onClick={ () => navigate('') }
            text="Connect" />          
        </VContainer>
      </VContainer>
    </VContainer>
  )
}