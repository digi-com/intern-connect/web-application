import {
  createRoot
} from 'react-dom/client';

import {
  router
} from './common'

import {
  RouterProvider
} from 'react-router-dom';

const rootNode = document.getElementById('root');

if (rootNode) {
  createRoot(rootNode)
    .render(<RouterProvider router={ router } />);
}