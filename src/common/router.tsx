import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  useRouteError
} from 'react-router-dom';

import {
  authRoutes,
  homePageRoutes
} from './routes'

import {
  App
} from '../App';
import { onboardingRoutes } from './routes/onboarding';

function ErrorBoundary() {
  let error = useRouteError();
  console.error(error);
  // Uncaught ReferenceError: path is not defined
  // To do:
  return <div>Something went wrong!</div>;
}

export const router = createBrowserRouter(

  createRoutesFromElements(

    <Route
      errorElement={ <ErrorBoundary /> }>

      <Route
        path='/'
        element={ <App /> }>
      
        { homePageRoutes }
        { onboardingRoutes }
      </Route>

      <Route path='/auth'>

        { authRoutes }
      </Route>
    </Route>
  )
);