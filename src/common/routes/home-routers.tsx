import {
  Route
} from 'react-router-dom';

import {
  Home
} from '../../ui';

export const homePageRoutes = (

  <Route path='/' element={ <Home /> } />
);
