import {
  Route
} from 'react-router-dom';

import {
  Onboarding
} from '../../ui';

export const onboardingRoutes = (

  <Route path='/onboarding' element={ <Onboarding /> } />
);
