import {
  Route
} from 'react-router-dom';

import {
  ForgotPassword,
  Login,
  Register,
  ResetPassword
} from '../../../ui';

export const authRoutes = (

  <Route>
    <Route path='login' element={ <Login /> } />
    <Route path='register' element={ <Register /> } />
    <Route path='forgot-password' element={ <ForgotPassword /> } />
    <Route path='reset-password' element={ <ResetPassword /> } />
  </Route>
)