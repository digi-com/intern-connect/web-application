import React from 'react';
import styled from 'styled-components';

export interface GridProps {
  $columns?: string;
  $rows?: string;
  $gap?: string;
  $rowGap?: string;
  $columnGap?: string;
  $justifyItems?: string;
  $alignItems?: string;
  $justifyContent?: string;
  $alignContent?: string;
  $areas?: string;
  $autoColumns?: string;
  $autoRows?: string;
  $autoFlow?: string;
}

export interface GridItemProps {
  $column?: string;
  $row?: string;
  $justifySelf?: string;
  $alignSelf?: string;
  $area?: string;
}

export const Grid = styled.div<GridProps>`
  display: grid;
  grid-template-columns: ${(props) => props.$columns};
  grid-template-rows: ${(props) => props.$rows};
  gap: ${(props) => props.$gap};
  row-gap: ${(props) => props.$rowGap};
  column-gap: ${(props) => props.$columnGap};
  justify-items: ${(props) => props.$justifyItems};
  align-items: ${(props) => props.$alignItems};
  justify-content: ${(props) => props.$justifyContent};
  align-content: ${(props) => props.$alignContent};
  grid-template-areas: ${(props) => props.$areas};
  grid-auto-columns: ${(props) => props.$autoColumns};
  grid-auto-rows: ${(props) => props.$autoRows};
  grid-auto-flow: ${(props) => props.$autoFlow};
`;

export const GridItem = styled.div<GridItemProps>`
  grid-column: ${(props) => props.$column};
  grid-row: ${(props) => props.$row};
  justify-self: ${(props) => props.$justifySelf};
  align-self: ${(props) => props.$alignSelf};
  grid-area: ${(props) => props.$area};
`;