import
  styled
from 'styled-components';

import {
  layoutProps,
  LayoutProps,
  boxModel,
  BoxModelProps,
  typography,
  TypographyProps,
  background,
  BackgroundProps,
  boxShadow,
  BoxShadowProps,
  positioning,
  PositioningProps,
  transitions,
  TransitionProps,
  pseudoClasses,
  PseudoClassesProps,
} from './styles/mixins';

export type StyledDivProps =
  LayoutProps &
  BoxModelProps &
  TypographyProps &
  BackgroundProps &
  BoxShadowProps &
  PositioningProps &
  TransitionProps &
  PseudoClassesProps;

export const StyledDiv = styled.div<StyledDivProps>`
  ${layoutProps}
  ${boxModel}
  ${typography}
  ${background}
  ${boxShadow}
  ${positioning}
  ${transitions}
  ${pseudoClasses}
`;