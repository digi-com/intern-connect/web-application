import
  React
from 'react';

import styled, {
  css
} from 'styled-components';

import {
  StyledDiv,
  StyledDivProps
} from './StyledDiv';

interface ContainerProps extends StyledDivProps {
  children?: React.ReactNode
}

const mediaQueries = {
  mobile: '480px',
  tablet: '600px',
  desktop: '1024px' 
}

export const mediaQueryStyles = css<ContainerProps>`
  @media (min-width: ${mediaQueries.mobile}) {
    display: ${props => props.$mobileDisplay ?? props.$display};
    align-content: ${props => props.$mobileAlignItems ?? props.$alignItems};
    justify-content: ${props => props.$mobileJustifyContent ?? props.$justifyContent};
    width: ${props => props.$mobileWidth ?? props.$width};
    height: ${props => props.$mobileHeight ?? props.$height};
  }

  @media (min-width: ${mediaQueries.tablet}) {
    display: ${props => props.$tabletDisplay ?? props.$display};
    align-content: ${props => props.$tabletAlignItems ?? props.$alignItems};
    justify-content: ${props => props.$tabletJustifyContent ?? props.$justifyContent};
    width: ${props => props.$tabletWidth ?? props.$width};
    height: ${props => props.$tabletHeight ?? props.$height};
  }

  @media (min-width: ${mediaQueries.desktop}) {
    display: ${props => props.$desktopDisplay ?? props.$display};
    align-content: ${props => props.$desktopAlignItems ?? props.$alignItems};
    justify-content: ${props => props.$desktopJustifyContent ?? props.$justifyContent};
    width: ${props => props.$desktopWidth ?? props.$width};
    height: ${props => props.$desktopHeight ?? props.$height};
  }
`;

export const Container = styled(StyledDiv)<ContainerProps>`
  ${ mediaQueryStyles }
`;

export const HContainer = styled(Container)<ContainerProps>`
  align-content: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
`;

export const HContainerC = styled(HContainer)<ContainerProps>`
  align-items: center;
`

export const HFillContainer = styled(HContainer)<ContainerProps>`
  width: 100%;
`;

export const HFillContainerC = styled(HFillContainer)<ContainerProps>`
  justify-content: center;
`;

export const VContainer = styled(Container)<ContainerProps>`
  align-content: flex-start;
  flex-direction: column;
`;

export const VFillContainer = styled(VContainer)<ContainerProps>`
  height: 100%;
`;

export const VFillContainerC = styled(VFillContainer)<ContainerProps>`
  align-items: center;
`;