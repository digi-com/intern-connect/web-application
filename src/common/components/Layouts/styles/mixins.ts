import { css, keyframes } from 'styled-components';

export interface LayoutProps {
  $alignContent?: string;
  $alignItems?: string;
  $mobileAlignItems?: string;
  $tabletAlignItems?: string;
  $desktopAlignItems?: string;
  $alignSelf?: string;
  $display?: string;
  $mobileDisplay?: string;
  $tabletDisplay?: string;
  $desktopDisplay?: string;
  $flex?: string;
  $flexBasis?: string;
  $flexDirection?: string;
  $flexShrink?: string;
  $flexGrow?: string;
  $flexWrap?: string;
  $justifyContent?: string;
  $mobileJustifyContent?: string;
  $tabletJustifyContent?: string;
  $desktopJustifyContent?: string;
  $order?: string;
}

export interface BoxModelProps {
  $animation?: string;
  $animate?: boolean;
  $boxSizing?: string;
  $gap?: string;
  $width?: string;
  $minWidth?: string;
  $maxWidth?: string;
  $mobileWidth?: string;
  $tabletWidth?: string;
  $desktopWidth?: string;
  $height?: string;
  $minHeight?: string;
  $maxHeight?: string;
  $mobileHeight?: string;
  $tabletHeight?: string;
  $desktopHeight?: string;
  $padding?: string;
  $paddingTop?: string;
  $paddingRight?: string;
  $paddingBottom?: string;
  $paddingLeft?: string;
  $margin?: string;
  $marginTop?: string;
  $marginRight?: string;
  $marginBottom?: string;
  $marginLeft?: string;
  $border?: string;
  $borderTop?: string;
  $borderRight?: string;
  $borderBottom?: string;
  $borderLeft?: string;
  $borderRadius?: string;
  $overflow?: string;
  $overflowX?: string;
  $overflowY?: string;
  $overflowWrap?: string;
  $textOverflow?: string;
  $whiteSpace?: string;
  $clipPath?: string;
  $opacity?: string;
  $outline?: string;
  $perspective?: string;
}

export interface TypographyProps {
  $fontFamily?: string;
  $fontSize?: string;
  $fontWeight?: string;
  $lineHeight?: string;
  $color?: string;
  $textAlign?: string;
}

export interface BackgroundProps {
  $background?: string;
  $backgroundColor?: string;
  $backgroundImage?: string;
  $backgroundSize?: string;
  $backgroundPosition?: string;
}

export interface BoxShadowProps {
  $boxShadow?: string;
}

export interface PositioningProps {
  $position?: string;
  $top?: string;
  $bottom?: string;
  $right?: string;
  $left?: string;
  $zIndex?: string;
}

export interface TransitionProps {
  $transition?: string;
}

export interface PseudoClassesProps {
  $beforeDisplay?: string;
  $beforePosition?: string;
  $hoverBackgroundColor?: string;
  $hoverCursor?: string;
  $hoverColor?: string;
  $beforeWidth?: string;
  $beforeHeight?: string;
  $beforeBackground?: string;
  $beforeTop?: string;
  $beforeLeft?: string;
}

export const layoutProps = css<LayoutProps>`
  align-content: ${(props) => props.$alignContent};
  align-items: ${(props) => props.$alignItems};
  align-self: ${(props) => props.$alignSelf};
  display: ${(props) => props.$display ?? 'flex'};
  flex: ${(props) => props.$flex};
  flex-basis: ${(props) => props.$flexBasis};
  flex-direction: ${(props) => props.$flexDirection};
  flex-shrink: ${(props) => props.$flexShrink};
  flex-grow: ${(props) => props.$flexGrow};
  flex-wrap: ${(props) => props.$flexWrap};
  justify-content: ${(props) => props.$justifyContent};
  order: ${(props) => props.$order};
`;

const defaultAnimation = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

export const boxModel = css<BoxModelProps>`
  animation: ${(props) => props.$animate ? `${ defaultAnimation } 0.5s ease-in-out` : props.$animation };
  box-sizing: ${(props) => props.$boxSizing};
  gap: ${(props) => props.$gap};
  width: ${(props) => props.$width};
  min-width: ${(props) => props.$minWidth};
  max-width: ${(props) => props.$maxWidth};
  height: ${(props) => props.$height};
  min-height: ${(props) => props.$minHeight};
  max-height: ${(props) => props.$maxHeight};
  padding: ${(props) => props.$padding};
  padding-top: ${(props) => props.$paddingTop};
  padding-right: ${(props) => props.$paddingRight};
  padding-bottom: ${(props) => props.$paddingBottom};
  padding-left: ${(props) => props.$paddingLeft};
  margin: ${(props) => props.$margin};
  margin-top: ${(props) => props.$marginTop};
  margin-right: ${(props) => props.$marginRight};
  margin-bottom: ${(props) => props.$marginBottom};
  margin-left: ${(props) => props.$marginLeft};
  border: ${(props) => props.$border};
  border-top: ${(props) => props.$borderTop};
  border-right: ${(props) => props.$borderRight};
  border-bottom: ${(props) => props.$borderBottom};
  border-left: ${(props) => props.$borderLeft};
  border-radius: ${(props) => props.$borderRadius};
  overflow: ${(props) => props.$overflow};
  overflow-x: ${(props) => props.$overflowX};
  overflow-y: ${(props) => props.$overflowY};
  overflow-wrap: ${(props) => props.$overflowWrap};
  text-overflow: ${(props) => props.$textOverflow};
  white-space: ${(props) => props.$whiteSpace};
  clip-path: ${(props) => props.$clipPath};
  opacity: ${(props) => props.$opacity};
  outline: ${(props) => props.$outline};
  perspective: ${(props) => props.$perspective};
`;

export const typography = css<TypographyProps>`
  font-family: ${(props) => props.$fontFamily};
  font-size: ${(props) => props.$fontSize};
  font-weight: ${(props) => props.$fontWeight};
  line-height: ${(props) => props.$lineHeight};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
`;

export const background = css<BackgroundProps>`
  background: ${(props) => props.$background};
  background-color: ${(props) => props.$backgroundColor};
  background-image: ${(props) => props.$backgroundImage ? `url(${props.$backgroundImage})` : undefined};
  background-size: ${(props) => props.$backgroundSize};
  background-position: ${(props) => props.$backgroundPosition};
`;

export const boxShadow = css<BoxShadowProps>`
  box-shadow: ${(props) => props.$boxShadow};
`;

export const positioning = css<PositioningProps>`
  position: ${(props) => props.$position};
  top: ${(props) => props.$top};
  bottom: ${(props) => props.$bottom};
  right: ${(props) => props.$right};
  left: ${(props) => props.$left};
  z-index: ${(props) => props.$zIndex};
`;

export const transitions = css<TransitionProps>`
  transition: ${(props) => props.$transition};
`;

export const pseudoClasses = css<PseudoClassesProps>`
  &:hover {
    background-color: ${(props) => props.$hoverBackgroundColor};
    cursor: ${(props) => props.$hoverCursor};
    color: ${(props) => props?.$hoverColor};
  }

  &::before {
    content: '';
    display: ${(props) => props.$beforeDisplay ?? 'block' };
    width: ${(props) => props.$beforeWidth};
    height: ${(props) => props.$beforeHeight};
    background: ${(props) => props.$beforeBackground};
    position: ${(props) => props.$beforePosition ?? 'absolute' };
    top: ${(props) => props.$beforeTop};
    left: ${(props) => props.$beforeLeft};
  }
`;
