import {
  styled,
  css
} from 'styled-components';

export interface StyledInputProps {
  $padding?: string,
  $margin?: string,
  $radius?: string
}

const inputStyles = css<StyledInputProps>`
  color: #898989;
  font-size: 16px;
  outline: none;
  border: 1px solid #D5D5D5;
  min-width: 300px;
  padding: ${ props => props?.$padding ?? '16px' };
  margin: ${ props => props?.$margin };
  border-radius: ${ props => props?.$radius ?? '10px' };
`;

export const Input = styled.input<StyledInputProps>`
  ${ inputStyles }
`;