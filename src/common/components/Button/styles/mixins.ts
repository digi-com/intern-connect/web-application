import { css } from 'styled-components';

export interface StyledButtonProps {
  $backgroundColor?: string;
  $hoverBackgroundColor?: string;
  $hoverBorderColor?: string;
  $hoverOpacity?: string;
  $hoverColor?: string;
  $hoverCursor?: string;
  $focusBackgroundColor?: string;
  $activeBackgroundColor?: string;
  $border?: string;
  $color?: string;
  $padding?: string;
  $textAlign?: string;
  $textDecoration?: string;
  $transition?: string;
  $fontSize?: string;
  $borderRadius?: string;
  $margin?: string;
  $width?: string;
  $height?: string;
}

export const hoverMixin = css<StyledButtonProps>`
  ${
    ({
      $hoverBackgroundColor,
      $hoverBorderColor,
      $hoverOpacity,
      $hoverColor,
      $hoverCursor
    }: StyledButtonProps) => css`
    &:hover {
      background-color: ${$hoverBackgroundColor};
      border-color: ${$hoverBorderColor};
      opacity: ${$hoverOpacity};
      color: ${$hoverColor};
      cursor: ${$hoverCursor};
    }
  `}
`;

export const focusMixin = (focusBackgroundColor: string) => css`
  &:focus {
    outline: none;
    box-shadow: 0 0 0 2px ${focusBackgroundColor};
  }
`;

export const activeMixin = (activeBackgroundColor: string) => css`
  &:active {
    background-color: ${activeBackgroundColor};
  }
`;