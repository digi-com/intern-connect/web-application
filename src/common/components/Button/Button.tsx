import React from 'react';
import StyledButton from './StyledButton';
import { StyledButtonProps } from './styles';

export interface ButtonProps extends StyledButtonProps {
  $onClick?: () => void;
  children?: React.ReactNode | string;
}

export const Button: React.FC<ButtonProps> = ({
  $backgroundColor = undefined,
  $hoverBackgroundColor = undefined,
  $hoverBorderColor = undefined,
  $hoverOpacity = undefined,
  $hoverColor = undefined,
  $hoverCursor = undefined,
  $focusBackgroundColor = undefined,
  $activeBackgroundColor = undefined,
  $border = undefined,
  $color = undefined,
  $padding = undefined,
  $textAlign = undefined,
  $textDecoration = undefined,
  $transition = undefined,
  $fontSize = undefined,
  $borderRadius = undefined,
  $margin = undefined,
  $width = undefined,
  $height = undefined,
  $onClick = undefined,
  children = undefined
}) => {
  return (
    
    <StyledButton
      $backgroundColor={$backgroundColor}
      $hoverBackgroundColor={$hoverBackgroundColor}
      $hoverBorderColor={$hoverBorderColor}
      $hoverOpacity={$hoverOpacity}
      $hoverColor={$hoverColor}
      $hoverCursor={$hoverCursor}
      $focusBackgroundColor={$focusBackgroundColor}
      $activeBackgroundColor={$activeBackgroundColor}
      $border={$border}
      $color={$color}
      $padding={$padding}
      $textAlign={$textAlign}
      $textDecoration={$textDecoration}
      $transition={$transition}
      $fontSize={$fontSize}
      $borderRadius={$borderRadius}
      $margin={$margin}
      $width={$width}
      $height={$height}
      onClick={$onClick}>

      { children }
    </StyledButton>
  );
};