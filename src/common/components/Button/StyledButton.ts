import styled from 'styled-components';
import { StyledButtonProps, hoverMixin } from './styles';

const StyledButton = styled.button<StyledButtonProps>`
  background-color: ${(props) => props.$backgroundColor};
  border: ${(props) => props.$border};
  color: ${(props) => props.$color};
  padding: ${(props) => props.$padding};
  text-align: ${(props) => props.$textAlign};
  text-decoration: ${(props) => props.$textDecoration};
  display: inline-block;
  font-size: ${(props) => props.$fontSize};
  margin: ${(props) => props.$margin};
  cursor: pointer;
  border-radius: ${(props) => props.$borderRadius};
  transition: ${(props) => props.$transition};
  width: ${(props) => props.$width};
  height: ${(props) => props.$height};

  ${hoverMixin};
`;

export default StyledButton;