import { Text } from "../Text";
import {
  Button
} from "./Button";

interface PrimaryButtonProps {
  text?: string,
  width?: string,
  onClick?: () => void
}

export const PrimaryButton: React.FC<PrimaryButtonProps> = ({
  text = '',
  width = undefined,
  onClick = () => {}
}) => {

  return (

    <Button
      $onClick={ onClick }
      $border="none"
      $padding="15px"
      $borderRadius="30px"
      $fontSize="16px"
      $color="#fff"
      $width={ width }
      $backgroundColor="#55CF86">

      { text }     
    </Button>
  );
}