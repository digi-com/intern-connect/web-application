import React from 'react';
import { StyledText, StyledTextProps } from './StyledText';

export interface TextProps extends StyledTextProps {
  children?: string,
}

export const Text: React.FC<TextProps> = ({
  $as = 'div',
  $display = undefined,
  $fontSize = undefined,
  $fontStyle = undefined,
  $fontFamily = undefined,
  $color = undefined,
  $textAlign = undefined,
  $fontWeight = undefined,
  $lineHeight = undefined,
  $margin = undefined,
  $padding = undefined,
  $textDecoration = undefined,
  children = undefined
}) => {

  return (

    <StyledText
      $as={ $as }
      $display={ $display }
      $fontSize={ $fontSize }
      $fontStyle={ $fontStyle }
      $fontFamily={ $fontFamily }
      $color={ $color }
      $textAlign={ $textAlign }
      $fontWeight={ $fontWeight }
      $lineHeight={ $lineHeight }
      $margin={ $margin }
      $padding={ $padding }
      $textDecoration={ $textDecoration }>

      { children }
    </StyledText>
  );
};