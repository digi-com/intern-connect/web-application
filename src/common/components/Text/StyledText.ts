import styled, { css } from 'styled-components';

export interface StyledTextProps {
  $as?: keyof JSX.IntrinsicElements,
  $display?: string,
  $fontSize?: string;
  $fontStyle?: string;
  $color?: string;
  $textAlign?: string;
  $fontWeight?: string;
  $fontFamily?: string;
  $lineHeight?: string;
  $margin?: string;
  $padding?: string;
  $textDecoration?: string;
};

const textStyles = css<StyledTextProps>`
  display: ${(props) => props.$display};
  font-size: ${(props) => props.$fontSize};
  font-style: ${(props) => props.$fontStyle};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  font-weight: ${(props) => props.$fontWeight};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  text-decoration: ${(props) => props.$textDecoration};
`;

export const StyledText = styled.div<StyledTextProps>`
  ${textStyles}
`;