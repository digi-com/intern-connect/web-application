import
  styled
from 'styled-components';

export interface StyledParagraphProps {
  $fontSize?: string;
  $color?: string;
  $fontWeight?: string;
  $fontFamily?: string;
  $margin?: string;
  $padding?: string;
  $lineHeight?: string;
}

export const StyledParagraph = styled.p<StyledParagraphProps>`
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  font-weight: ${(props) => props.$fontWeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
`;