import
  React
from 'react';

import {
  StyledParagraph,
  StyledParagraphProps
} from './StyledParagraph';

export interface ParagraphProps extends StyledParagraphProps {
  text: string
}

export const P: React.FC<ParagraphProps> = ({
  text = '',
  $color = undefined,
  $fontSize = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $margin = undefined,
  $padding = undefined,
  $lineHeight = undefined
}) => {

  return (

    <StyledParagraph
      $color={ $color }
      $fontSize={ $fontSize }
      $fontWeight={ $fontWeight }
      $fontFamily={ $fontFamily }
      $margin={ $margin }
      $padding={ $padding }
      $lineHeight={ $lineHeight }>

      { text }
    </StyledParagraph>
  )
}