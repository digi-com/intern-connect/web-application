import styled from 'styled-components';

export interface StyledHeadingProps {
  $fontSize?: string;
  $color?: string;
  $textAlign?: string;
  $textTransform?: string;
  $fontWeight?: string;
  $fontFamily?: string;
  $margin?: string;
  $padding?: string;
  $lineHeight?: string;
}

export const StyledH1 = styled.h1<StyledHeadingProps>`
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  text-transform: ${(props) => props.$textTransform};
  font-weight: ${(props) => props.$fontWeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
`;

export const StyledH2 = styled.h2<StyledHeadingProps>`
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  text-transform: ${(props) => props.$textTransform};
  font-weight: ${(props) => props.$fontWeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
`;

export const StyledH4 = styled.h4<StyledHeadingProps>`
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  text-transform: ${(props) => props.$textTransform};
  font-weight: ${(props) => props.$fontWeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
`;

export const StyledH5 = styled.h5<StyledHeadingProps>`
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  text-transform: ${(props) => props.$textTransform};
  font-weight: ${(props) => props.$fontWeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
`;