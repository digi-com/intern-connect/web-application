import
  React
from 'react';

import {
  StyledH1,
  StyledH2,
  StyledH4,
  StyledH5,
  StyledHeadingProps
} from './StyledHeadings';

export interface HeadingProps extends StyledHeadingProps {
  text: string
}

export const H1: React.FC<HeadingProps> = ({
  text = '',
  $color = undefined,
  $textAlign = undefined,
  $textTransform = undefined,
  $fontSize = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $margin = undefined,
  $padding = undefined,
  $lineHeight = undefined
}) => {

  return (

    <StyledH1
      $color={ $color }
      $textAlign={ $textAlign }
      $textTransform={ $textTransform }
      $fontSize={ $fontSize }
      $fontWeight={ $fontWeight }
      $fontFamily={ $fontFamily }
      $margin={ $margin }
      $padding={ $padding }
      $lineHeight={ $lineHeight }>

      { text }
    </StyledH1>
  )
}

export const H2: React.FC<HeadingProps> = ({
  text = '',
  $color = undefined,
  $textAlign = undefined,
  $textTransform = undefined,
  $fontSize = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $margin = undefined,
  $padding = undefined,
  $lineHeight = undefined
}) => {

  return (

    <StyledH2
      $color={ $color }
      $textAlign={ $textAlign }
      $textTransform={ $textTransform }
      $fontSize={ $fontSize }
      $fontWeight={ $fontWeight }
      $fontFamily={ $fontFamily }
      $margin={ $margin }
      $padding={ $padding }
      $lineHeight={ $lineHeight }>

      { text }
    </StyledH2>
  )
}

export const H4: React.FC<HeadingProps> = ({
  text = '',
  $color = undefined,
  $textAlign = undefined,
  $textTransform = undefined,
  $fontSize = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $margin = undefined,
  $padding = undefined,
  $lineHeight = undefined
}) => {

  return (

    <StyledH4
      $color={ $color }
      $textAlign={ $textAlign }
      $textTransform={ $textTransform }
      $fontSize={ $fontSize }
      $fontWeight={ $fontWeight }
      $fontFamily={ $fontFamily }
      $margin={ $margin }
      $padding={ $padding }
      $lineHeight={ $lineHeight }>

      { text }
    </StyledH4>
  )
}

export const H5: React.FC<HeadingProps> = ({
  text = '',
  $color = undefined,
  $textAlign = undefined,
  $textTransform = undefined,
  $fontSize = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $margin = undefined,
  $padding = undefined,
  $lineHeight = undefined
}) => {

  return (

    <StyledH5
      $color={ $color }
      $textAlign={ $textAlign }
      $textTransform={ $textTransform }
      $fontSize={ $fontSize }
      $fontWeight={ $fontWeight }
      $fontFamily={ $fontFamily }
      $margin={ $margin }
      $padding={ $padding }
      $lineHeight={ $lineHeight }>

      { text }
    </StyledH5>
  )
}