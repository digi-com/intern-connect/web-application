import
  React
from 'react';

import {
  NavLinkProps as NLP
} from 'react-router-dom';

import {
  StyledNavList,
  StyledNavLinkProps
} from './StyledNavLink';

type NavLinkProps = NLP & StyledNavLinkProps;

export const NavLink: React.FC<NavLinkProps> = ({
  $display = undefined,
  $fontSize = undefined,
  $background = undefined,
  $activeBackground = undefined,
  $hoverBackground = undefined,
  $border = undefined,
  $activeBorder = undefined,
  $hoverBorder = undefined,
  $color = undefined,
  $hoverColor = undefined,
  $activeColor = undefined,
  $textAlign = undefined,
  $textDecoration = undefined,
  $fontWeight = undefined,
  $fontFamily = undefined,
  $lineHeight = undefined,
  $margin = undefined,
  $padding = undefined,
  to,
  onClick = undefined,
  children = undefined
}) => {

  return (

    <StyledNavList
      onClick={onClick}
      $display={$display}
      $fontSize={$fontSize}
      $background={$background}
      $activeBackground={$activeBackground}
      $hoverBackground={$hoverBackground}
      $border={$border}
      $activeBorder={$activeBorder}
      $hoverBorder={$hoverBorder}
      $color={$color}
      $hoverColor={$hoverColor}
      $activeColor={$activeColor}
      $textAlign={$textAlign}
      $textDecoration={$textDecoration}
      $fontWeight={$fontWeight}
      $fontFamily={$fontFamily}
      $lineHeight={$lineHeight}
      $margin={$margin}
      $padding={$padding}
      to={to}>

      { children }
    </StyledNavList>
  );
};