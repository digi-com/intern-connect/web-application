import styled, { css } from 'styled-components';
import { NavLink } from 'react-router-dom';

export interface StyledNavLinkProps {
  $display?: string,
  $fontSize?: string;
  $background?: string;
  $activeBackground?: string;
  $hoverBackground?: string;
  $border?: string;
  $activeBorder?: string;
  $hoverBorder?: string;
  $color?: string;
  $hoverColor?: string;
  $activeColor?: string;
  $textAlign?: string;
  $textDecoration?: string;
  $fontWeight?: string;
  $fontFamily?: string;
  $lineHeight?: string;
  $margin?: string;
  $padding?: string;
};

const navLinkStyles = css<StyledNavLinkProps>`
  border: ${(props) => props.$border};
  background: ${(props) => props.$background};
  display: ${(props) => props.$display};
  font-size: ${(props) => props.$fontSize};
  color: ${(props) => props.$color};
  text-align: ${(props) => props.$textAlign};
  text-decoration: ${(props) => props.$textDecoration};
  font-weight: ${(props) => props.$fontWeight};
  font-family: ${(props) => props.$fontFamily};
  line-height: ${(props) => props.$lineHeight};
  margin: ${(props) => props.$margin};
  padding: ${(props) => props.$padding};
  &:hover {
    border: ${(props) => props?.$hoverBackground ?? props.$border};
    background: ${(props) => props.$hoverBackground ?? props.$background};
    color: ${(props) => props.$hoverColor ?? props.$color};
  }
  &:active {
    border: ${(props) => props?.$activeBackground ?? props.$border};
    background: ${(props) => props.$activeBackground ?? props.$background};
    color: ${(props) => props?.$activeColor ?? props.$color};
  }
`;

export const StyledNavList = styled(NavLink)<StyledNavLinkProps>`
  ${ navLinkStyles }
`;